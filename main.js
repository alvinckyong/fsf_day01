/**
 * Created by alvinyong on 27/6/16.
 */

//Load Express Module
var express = require("express");
//Create an instance of Express Application
var app = express();

//Process command line arguments
// -p port
// -d dir
var port = 3000;
var staticDir = __dirname + "/public";
var version =1;
var i =2;
while (i < process.argv.length) {
    console.info("in the loop");
    switch (process.argv[i]) {
        case "-v":
            console.info("Version : %d", version);
            break;
        case "-p":
            i = i + 1;
            port = process.argv[i];
            break;
        case "-d":
            i = i + 1;
            staticDir = process.argv[i];
            break;

        default:
    }
    i += 1;
}

//Use public directory for static files
app.use(
    express.static(__dirname + "/public")
);

console.info("__dirname: %s", __dirname);

for (var i = 0; i < process.argv.length; i++)
    console.info("argv: %d = %s", i, process.argv[i]);

//Set a port
app.set("port",
    process.argv[2] || 3000);

//Start the server on port
app.listen(
    app.get("port"),
    function(){
        console.info("Application started on port %d", app.get("port"));
        console.info("Application started on port " + app.get("port"));
        console.info("Application written by " + 'alvin' +
            ", started on port" + app.get("port"));
        console.info("Application writen by %s on port %d", 'alvin', app.get("port"));
        console.info("%s started on port %d", process.argv[1], app.get("port"));
    }
)